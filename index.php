<?php
/**
 * Variable contenant la racine du serveur.
 * Utilise pour éviter de modifier le  include path d'apache
 */
$DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];

include $DOCUMENT_ROOT . "/app/utils/includes.php";


$view_path = $DOCUMENT_ROOT . "/app/views/beers/list.phtml";

include $DOCUMENT_ROOT . "/app/views/layouts/layout.phtml";

?>